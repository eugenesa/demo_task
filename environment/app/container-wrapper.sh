#!/bin/bash

function initial_deploy {
  if [[ "$1" -gt 0 ]]
  then
    echo "Initial deploy for $1 replicas"
    for (( i = 1; i <= $1; i++)); do
      echo "Deploy container $i"
      docker run -d --name demo_app_1_${RANDOM} --network demo_backend -e APP_MYSQL_HOST=mariadb-master demo_app:1
    done
  else
    echo "Number of pods less then 1, nothing to deploy"
    exit 1
  fi
}

function rolling_update {
  NEW_CONTAINER_VERSION=$1
  # Dirty hack to determine containers id's registered in HAProxy
  old_containers=`docker ps -f name="demo_app" -q`

  for container in ${old_containers}; do
    echo "${container} is running"
    # Call /shutdown enpoint to deregister container from HAProxy and kill it
    # sleep few seconds to reload haproxy conf
    curl http://${container}:5000/shutdown && sleep 5 
    # Housekeeping 
    docker container rm --force ${container}
    # Up new version
    docker run -d --name demo_app_${NEW_CONTAINER_VERSION}_${RANDOM} --network demo_backend -e APP_MYSQL_HOST=mariadb-master demo_app:${NEW_CONTAINER_VERSION}
    # sleep few seconds during app startup 
    sleep 3
  done
  echo "Rolling update finished"
}

function cleanup {
  # Remove all unused app containers
  old_containers=`docker ps -f name="demo_app" -q`
  for container in ${old_containers}; do
    curl http://${container}:5000/shutdown && sleep 5
    docker container rm --force ${container}
  done
  old_images=`docker image ls | grep demo_app | awk {'print $3'}`
  for image in ${old_images}; do 
    docker image rm --force ${image}
  done
  echo "Cleanup finished"
}

if [ $1 == "initial_deploy" ]; then
  echo "Processing $1 option"
  initial_deploy $2
elif [ $1 == "rolling_update" ]; then
  echo "Processing $1 option"
  rolling_update $2
elif [ $1 == "cleanup" ]; then
  echo "Processing $1 option"
  cleanup
else
  echo "Wrong option. Exit"
  exit 1
fi
