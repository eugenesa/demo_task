from flask import (Flask, request, render_template, jsonify, make_response,
                   url_for, session, redirect)
from hashlib import md5 as md5
from base64 import b64encode
from urllib.error import HTTPError, URLError
from signal import SIGTERM
import pymysql
import os
import requests

# https://pymysql.readthedocs.io/en/latest/user/examples.html

mysql_host = os.getenv('APP_MYSQL_HOST', 'localhost')
haproxy_api_baseurl = 'http://nginx/api_haproxy'
hostname = os.uname().nodename

db_connection = pymysql.connect(host=mysql_host,
                                user='demo_app',
                                password='demo_app',
                                db='demo_app',
                                autocommit=True)

app = Flask(__name__, static_url_path='/static')
app.secret_key = "changemehere!!!"


def haproxy_register(hostname=None):
    # need current haproxy config version to make changes via api
    url = "%s%s" % (haproxy_api_baseurl,
                    '/v1/services/haproxy/configuration/servers?backend=app')
    r = requests.get(url, auth=('admin', 'admin'))
    current_haproxy_config = r.json()['_version']
    r.close()
    url2 = "%s%s?backend=app&version=%s" % (
        haproxy_api_baseurl, '/v1/services/haproxy/configuration/servers',
        current_haproxy_config)
    data = {
        "name": hostname,
        "port": 5000,
        "check": "enabled",
        "inter": 3000,
        "address": hostname,
    }
    r2 = requests.post(url=url2, json=data, auth=('admin', 'admin'))
    return r2.status_code


def haproxy_deregister(hostname=None):
    url = "%s%s" % (haproxy_api_baseurl,
                    '/v1/services/haproxy/configuration/servers?backend=app')
    r = requests.get(url, auth=('admin', 'admin'))
    current_haproxy_config = r.json()['_version']
    r.close()
    url2 = "%s%s%s?backend=app&version=%s&force_reload=true" % (
        haproxy_api_baseurl, '/v1/services/haproxy/configuration/servers/',
        hostname, current_haproxy_config)
    r2 = requests.delete(url=url2, auth=('admin', 'admin'))
    return r2.status_code


def initial_sql_migration():
    _sql_file_path = os.path.join(os.getcwd(), 'init.sql')
    if os.path.exists(_sql_file_path):
        cursor = db_connection.cursor()
        with open(_sql_file_path) as sql_file:
            _tmp = sql_file.read().split(';')[:-1]
        for i in _tmp:
            q = i.replace('\n', '') + ';'
            cursor.execute(q)
        cursor.close()


def log_in(request_data):
    username = request_data['username']
    password = request_data['password']
    if len(username) > 0 and len(password) > 0:
        cursor = db_connection.cursor()
        q = "SELECT COUNT(*) FROM `users` WHERE `username` = \"%s\";" % (
            username)
        cursor.execute(q)
        if cursor.fetchone()[0] > 0:
            q = "SELECT `password` FROM `users` WHERE `username` = \"%s\";" % (
                username)
            cursor.execute(q)
            pw_hash = cursor.fetchone()[0]
            cursor.close()
            if md5(password.encode('utf-8')).hexdigest() == pw_hash:
                pass
            else:
                raise ValueError('Password is incorrect')
        else:
            raise ValueError('User not found')
    else:
        raise ValueError('Invalid username or password')
    return username


def get_data():
    cursor = db_connection.cursor()
    q = "SELECT * FROM `data`;"
    cursor.execute(q)
    _data = cursor.fetchall()
    cursor.close()
    return _data


def save_data(data=None):
    if data:
        q = "INSERT INTO `data` (`id`, `data`) VALUES (NULL, \"%s\");" % data
        cursor = db_connection.cursor()
        cursor.execute(q)
        cursor.close()
    else:
        raise ValueError("No input. Try add something before save.")


def update_data(record_id=None, data=None):
    if record_id and data:
        q = "UPDATE `data` SET `data`=\"%s\" WHERE `id`=%s;" % (data,
                                                                record_id)
        cursor = db_connection.cursor()
        cursor.execute(q)
        cursor.close()
    else:
        raise ValueError("Wrong input - incorrect record_id")


def delete_data(record_id):
    if record_id and int(record_id):
        q = "DELETE FROM `data` WHERE `id`=%s;" % int(record_id)
        cursor = db_connection.cursor()
        cursor.execute(q)
        cursor.close()
    else:
        raise ValueError("Empty or wrong record id (should be integer)")


@app.route('/health', methods=['GET', 'HEAD'])
def health():
    return make_response(jsonify({'OK': 'ok'}), '200')


@app.route('/shutdown', methods=['GET'])
def shutdown():
    haproxy_deregister(hostname=hostname)
    os.kill(os.getpid(), SIGTERM)
    return None


@app.route('/', methods=['GET', 'POST'])
def index():
    msg = None
    version = '1.0'
    if 'username' not in session:
        return redirect(url_for('login'))
    if request.method == 'POST':
        _newdata = request.form.get('data')
        try:
            save_data(_newdata)
        except ValueError as e:
            return render_template('index.html',
                                   session=session,
                                   data=get_data(),
                                   hostname=hostname,
                                   version=version,
                                   msg=str(e))
        return redirect(url_for('index'))
    return render_template('index.html',
                           session=session,
                           data=get_data(),
                           hostname=hostname,
                           version=version,
                           msg=msg)


@app.route('/delete_record/<int:record_id>')
def delete_record(record_id=None):
    msg = None
    if 'username' not in session:
        return redirect(url_for('login'))
    if record_id:
        try:
            delete_data(record_id=record_id)
        except ValueError as e:
            render_template('index.html',
                            session=session,
                            data=get_data(),
                            msg=str(e))
        return redirect(url_for('index'))


@app.route('/edit_record/<int:record_id>', methods=['POST'])
def edit_record(record_id=None):
    msg = None
    if 'username' not in session:
        return redirect(url_for('login'))
    if record_id and request.method == 'POST':
        _newdata = request.form.get('data')
        try:
            update_data(record_id=record_id, data=_newdata)
        except ValueError as e:
            render_template('index.html',
                            session=session,
                            data=get_data(),
                            msg=str(e))
        return redirect(url_for('index'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    msg = None
    if 'username' in session:
        return redirect(url_for('index'))
    if request.method == 'POST':
        try:
            session['username'] = log_in(request.form.to_dict(flat=True))
        except ValueError as e:
            return render_template('login.html', msg=str(e))
        return redirect(url_for('index'))
    return render_template('login.html', msg=msg)


@app.route('/logout')
def logout():
    session.pop('username', None)
    return redirect(url_for('login'))


if __name__ == '__main__':
    initial_sql_migration()
    haproxy_register(hostname=hostname)
    app.run(host='0.0.0.0', debug=False)

