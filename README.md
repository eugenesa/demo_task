# Demo 

## Components
- Jenkins
Customized dockerized jenkins installation. Includes ldap authorization and several plugins included.
Ready to use out of the box. All necessary data stored in jenkins_home_latest.tar.gz and will be imported at startup.

- OpenLDAP + phpOpenLDAP web-interface

- mariadb
Datastore backend for demo application. Working in master-slave mode.

- nginx + HAProxy
nginx interacts only as reverse proxy for application. HAProxy used as load balancer.

- demo application
Flask application

### Requirements
Docker and docker-compose

### Quick start
```bash
git clone https://gitlab.com/eugenesa/demo_task.git

cd demo_task/environment

# This step needed once, to determine docker executable. Because we have it in diffirent paths on MacOS/Linux distros.
echo "DOCKER_BIN"=`which docker` >> .env 

docker-compose up -d --build
```

### Additional info
After all services from compose is up, naviagte to your machines main ip address to access demo application. Credentials: admin/admin

Jenkins installation available in /jenkins location. Credentials (ldap backend): jenkinsadmin/123

Also please recheck 'Jenkins URL' at jenkins configuration, need for correct work of 'Pipeline Input plugin' in this demo setup.
